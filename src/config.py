import os

UPLOAD_FOLDER = os.getenv('UPLOAD_FOLDER', 'symbols')
DOWNLOAD_FOLDER = os.getenv('DOWNLOAD_FOLDER', 'results')