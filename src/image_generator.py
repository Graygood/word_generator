import sys
import os
from PIL import Image
import random
import json
from flask import current_app as app

class ImageGenerator():
    def __init__(self):
        self.path = 'symbols'
        self.symbols = self.get_all_symbols()

    def get_all_symbols(self):
        files = (file for file in os.listdir(self.path)
         if os.path.isfile(os.path.join(self.path, file)))

        symbol_dict = {}
        for file in files:
            if not symbol_dict.get(file[0]):
                symbol_dict.update({file[0] : []})
            symbol_dict[file[0]].append(file)

        self.symbols = symbol_dict
        return symbol_dict

    def get_symbols_for_generation(self, text):
        images = []
        no_symbols = []
        for sym in text:
            try:
                images.append(f'{self.path}/' + random.choice(self.symbols[sym]))
            except:
                no_symbols.append(sym)

        return images, no_symbols

    def generate_image(self, text):
        images, no_symbols = self.get_symbols_for_generation(text)

        if no_symbols != []:
            error = 'Current symbols does not exist in dataset: ' + ', '.join(no_symbols)
            response = {'success': False, 'error': error}
            response = json.dumps(response, ensure_ascii=False).encode('utf-8')
            return json.loads(response)

        images = [Image.open(x) for x in images]
        widths, heights = zip(*(i.size for i in images))

        total_width = sum(widths)
        max_height = max(heights)

        new_im = Image.new('RGB', (total_width, max_height), (255, 255, 255, 0))

        x_offset = 0
        for im in images:
          new_im.paste(im, (x_offset,int((max_height - im.size[1])/2)), mask=im)
          x_offset += im.size[0]

        # Lowering image size
        new_im.thumbnail((1024, 1024), Image.ANTIALIAS)

        image_name = f'{app.config["DOWNLOAD_FOLDER"]}/{text}.png'
        new_im.save(image_name)

        return {'success': True, 'result' : image_name}