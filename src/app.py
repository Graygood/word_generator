from flask import Flask, request, send_file, render_template, send_from_directory
import json
from image_generator import ImageGenerator
from utils import generate_filename
import os

app = Flask(__name__)
app.config.from_object('config')
generator = ImageGenerator()

@app.route('/generate_image', methods=["POST"])
def generate_image():
    text = request.values.get('text')
    image = generator.generate_image(text)
    if image.get('success'):
        return render_template("photo.html", image = image['result'])
    else:
        return json.dumps(image, ensure_ascii=False).encode('utf-8').decode('utf-8')

@app.route('/all_symbols')
def all_symbols():
    files = (file for file in os.listdir('symbols')
     if os.path.isfile(os.path.join('symbols', file)))
    files = list(files)

    return render_template("content.html", len = len(files), files = files)

@app.route('/add_symbols', methods=["POST"])
def add_symbols():
    uploaded_files = request.files.getlist("file")
    for file in uploaded_files:
        if file.filename.endswith('\"'):
            file.filename = file.filename[:-1]
        if os.path.isfile(os.path.join(app.config['UPLOAD_FOLDER'], file.filename)):
            file.filename = generate_filename(file.filename, 1)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
    generator.get_all_symbols()
    return json.dumps({'success': True})

@app.route('/delete_image/<path:path>', methods=["POST"])
def delete_image(path):
    os.remove(os.path.join('symbols', path))
    return json.dumps({'success': True})

@app.route('/results/<path:path>')
def images(path):
    return send_from_directory('results', path)

@app.route('/symbols/<path:path>')
def symbols(path):
    return send_from_directory('symbols', path)

if __name__ == '__main__':
    app.run()