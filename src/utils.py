import os
from flask import current_app as app

def generate_filename(filename, order):
    name, extension = filename.split('.')
    new_filename = name + '_' + str(order) + '.' + extension
    if os.path.isfile(os.path.join(app.config['UPLOAD_FOLDER'], new_filename)):
        order += 1
        return generate_filename(filename, order)
    return new_filename